
package aplicacaoJava;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;


public class TelaJogo {
    private int jogadorAtual = 0;
    private JFrame janela;
    private JPanel matriz;
   
    private JTextField numero1;
    private JTextField numero2;
    private JTextField numero3;
    private JTextField numero4;
    private JTextField numero5;
    private JTextField numero6;
    private JTextField numero7;
    private JTextField numero8;
    private JTextField numero9;
    private String nomeJogador1;
    private String nomeJogador2;
    
    public void construir(String nomeJogador1, String nomeJogador2){
        
        this.nomeJogador1 = nomeJogador1;
        this.nomeJogador2 = nomeJogador2;
       
        janela = new JFrame(nomeJogador1+" X "+nomeJogador2);
        janela.setBounds(750, 300, 350, 360);
        janela.setResizable(false);
        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        numero1 = new JTextField();
        numero1.setBackground(Color.white);
        
        numero2 = new JTextField();
        numero2.setBackground(Color.white);
        
        numero3 = new JTextField();
        numero3.setBackground(Color.white);
        
        numero4 = new JTextField();
        numero4.setBackground(Color.white);
        
        numero5 = new JTextField();
        numero5.setBackground(Color.white);
        
        numero6 = new JTextField();
        numero6.setBackground(Color.white);
        
        numero7 = new JTextField();
        numero7.setBackground(Color.white);
        
        numero8 = new JTextField();
        numero8.setBackground(Color.white);
        
        numero9 = new JTextField();
        numero9.setBackground(Color.white);
        
        numero1.setEditable(false);
        numero2.setEditable(false);
        numero3.setEditable(false);
        numero4.setEditable(false);
        numero5.setEditable(false);
        numero6.setEditable(false);
        numero7.setEditable(false);
        numero8.setEditable(false);
        numero9.setEditable(false);
        
        matriz = new JPanel(new GridLayout(3, 3));
        
        matriz.add(numero7);
        matriz.add(numero8);
        matriz.add(numero9);
        
        matriz.add(numero4);
        matriz.add(numero5);
        matriz.add(numero6);
        
        matriz.add(numero1);
        matriz.add(numero2);
        matriz.add(numero3);
                
        
        janela.getContentPane().add(matriz);
        
        acoesTeclado() ;
        acoesDoMouse();
        condicoesParaGanhar();
        
        janela.setVisible(true);
    }
    
    private void botao(JTextField posicao){
        if(posicao.getText().equals("") ){
            if(jogadorAtual % 2 == 0){
                posicao.setText("X");
            }
            else{
                posicao.setText("O");
            }
            jogadorAtual++;
        }
        condicoesParaGanhar();
    }
   
    public void acoesTeclado() 
    {
        janela.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                
            }
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_7) {
                    botao(numero7);
                }
                if (e.getKeyCode() == KeyEvent.VK_1) {
                    botao(numero1);
                }
                if (e.getKeyCode() == KeyEvent.VK_2) {
                    botao(numero2);
                }
                if (e.getKeyCode() == KeyEvent.VK_3) {
                    botao(numero3);
                }
                if (e.getKeyCode() == KeyEvent.VK_4) {
                    botao(numero4);
                }
                if (e.getKeyCode() == KeyEvent.VK_5) {
                    botao(numero5);
                }
                if (e.getKeyCode() == KeyEvent.VK_6) {
                    botao(numero6);
                }
                if (e.getKeyCode() == KeyEvent.VK_8) {
                    botao(numero8);
                }
                if (e.getKeyCode() == KeyEvent.VK_9) {
                    botao(numero9);
                }
            }
              @Override
            public void keyReleased(KeyEvent e) {
                
            }
        });
    }
    
    private void acoesDoMouse(){
        numero1.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero1);
                janela.requestFocus();
            }
        });
        numero2.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero2);
                janela.requestFocus();
            }
        });
        numero3.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero3);
                janela.requestFocus();
            }
        });
        numero4.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero4);
                janela.requestFocus();
            }
        });
        numero5.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero5);
                janela.requestFocus();
            }
        });
        numero6.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero6);
                janela.requestFocus();
            }
        });
        numero7.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero7);
                janela.requestFocus();
            }
        });
        numero8.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero8);
                janela.requestFocus();
            }
        });
        numero9.addMouseListener(new MouseAdapter() {
            
            public void mouseClicked(MouseEvent e) {
                botao(numero9);
                janela.requestFocus();
            }
        });
    }
    
    public void travarJogo(){
        numero1.setEnabled(false);
        numero2.setEnabled(false);
        numero3.setEnabled(false);
        numero4.setEnabled(false);
        numero5.setEnabled(false);
        numero6.setEnabled(false);
        numero7.setEnabled(false);
        numero8.setEnabled(false);
        numero9.setEnabled(false);
    }
    
    public void pintarLinhaGanhadora(JTextField ganhou1, JTextField ganhou2, JTextField ganhou3){
        ganhou1.setBackground(Color.RED);
        ganhou2.setBackground(Color.RED);
        ganhou3.setBackground(Color.RED);   
    }
    
    private void condicoesParaGanhar(){
        //Quando o X ganhar
        if(numero1.getText().equals("X")  && numero2.getText().equals("X")  &&  numero3.getText().equals("X")){
            pintarLinhaGanhadora(numero1, numero2, numero3);
            travarJogo();
        }
        if(numero4.getText().equals("X") && numero5.getText().equals("X") && numero6.getText().equals("X")){
            pintarLinhaGanhadora(numero4, numero5, numero6);
            travarJogo();
        }
        if(numero7.getText().equals("X") && numero8.getText().equals("X") && numero9.getText().equals("X") ){
            pintarLinhaGanhadora(numero7, numero8, numero9);
            travarJogo();
        }
        if(numero1.getText().equals("X") &&  numero4.getText().equals("X") && numero7.getText().equals("X") ){
            pintarLinhaGanhadora(numero1, numero4, numero7);
            travarJogo();
        }
        if(numero2.getText().equals("X") && numero5.getText().equals("X") && numero8.getText().equals("X")){
            pintarLinhaGanhadora(numero2, numero5, numero8);
            travarJogo();
        }
        if(numero3.getText().equals("X") && numero6.getText().equals("X") && numero9.getText().equals("X")){
            pintarLinhaGanhadora(numero3, numero6, numero9);
            travarJogo();
        }
        if(numero1.getText().equals("X") && numero5.getText().equals("X") && numero9.getText().equals("X") ){
            pintarLinhaGanhadora(numero1, numero5, numero9);
            travarJogo();
        }
        if(numero3.getText().equals("X") && numero5.getText().equals("X") && numero7.getText().equals("X") ){
            pintarLinhaGanhadora(numero3, numero5, numero7);
            travarJogo();
        }   
        //Quando o O ganhar
        if(numero1.getText().equals("O")  && numero2.getText().equals("O")  &&  numero3.getText().equals("O")){
            pintarLinhaGanhadora(numero1, numero2, numero3);
            travarJogo();
        }
        if(numero4.getText().equals("O") && numero5.getText().equals("O") && numero6.getText().equals("O")){
            pintarLinhaGanhadora(numero4, numero5, numero6);
            travarJogo();
        }
        if(numero7.getText().equals("O") && numero8.getText().equals("O") && numero9.getText().equals("O") ){
            pintarLinhaGanhadora(numero7, numero8, numero9);
            travarJogo();
        }
        if(numero1.getText().equals("O") &&  numero4.getText().equals("O") && numero7.getText().equals("O") ){
            pintarLinhaGanhadora(numero1, numero4, numero7);
            travarJogo();
        }
        if(numero2.getText().equals("O") && numero5.getText().equals("O") && numero8.getText().equals("O")){
            pintarLinhaGanhadora(numero2, numero5, numero8);
            travarJogo();
        }
        if(numero3.getText().equals("O") && numero6.getText().equals("O") && numero9.getText().equals("O")){
            pintarLinhaGanhadora(numero3, numero6, numero9);
            travarJogo();
        }
        if(numero1.getText().equals("O") && numero5.getText().equals("O") && numero9.getText().equals("O") ){
            pintarLinhaGanhadora(numero1, numero5, numero9);
            travarJogo();
        }
        if(numero3.getText().equals("O") && numero5.getText().equals("O") && numero7.getText().equals("O") ){
            pintarLinhaGanhadora(numero3, numero5, numero7);
            travarJogo();
        }
    }  
}
