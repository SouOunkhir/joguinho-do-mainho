package aplicacaoJava;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class TelaPrincipal {

    private JFrame janela;
    private JPanel painelGeral;
    private JPanel painelJogador1;
    private JPanel painelJogador2;
    private JPanel painelComeca;
    private JPanel painelJogar;
    private JTextField textoNome1;
    private JTextField textoNome2;
    private JLabel nomeLabel1;
    private JLabel nomeLabel2;
    private JLabel quemComeca;
    private JRadioButton radio1;
    private JRadioButton radio2;
    private ButtonGroup group;
    private JButton botaoJogar;
    private String jogador1;
    private String jogador2;

    public void construir(){
        janela = new JFrame("");
        janela.setBounds(750, 300, 700, 350);
        janela.getContentPane().setLayout(new FlowLayout());
        painelGeral = new JPanel(new GridLayout(6, 1));
        painelJogador1 = new JPanel();
        painelJogador2 = new JPanel();
        painelComeca = new JPanel();
        painelJogar = new JPanel();

        radio1 = new JRadioButton("Jogador1");
        radio2 = new JRadioButton("Jogador2");
        nomeLabel1 = new JLabel("Jogador 1: ");
        nomeLabel2 = new JLabel("Jogador 2: ");
        quemComeca = new JLabel("Quem começa? ");
        textoNome1 = new JTextField();
        textoNome2 = new JTextField();
        botaoJogar = new JButton("Jogar");

        textoNome1.setPreferredSize(new Dimension(250, 30));
        textoNome2.setPreferredSize(new Dimension(250, 30));

        painelJogar.add(botaoJogar);

        painelJogador1.add(nomeLabel1);
        painelJogador1.add(textoNome1);
        painelJogador2.add(nomeLabel2);
        painelJogador2.add(textoNome2);

        group = new ButtonGroup();

        group.add(radio1);
        group.add(radio2);

        painelGeral.add(painelJogador1);
        painelGeral.add(painelJogador2);
        painelComeca.add(quemComeca);
        painelGeral.add(painelComeca);
        painelGeral.add(radio1);
        painelGeral.add(radio2);
        painelGeral.add(painelJogar);

        janela.getContentPane().add(painelGeral);

        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        acoesDosBotoes();
        janela.setVisible(true);
    }

    public void acoesDosBotoes(){
        botaoJogar.addActionListener(new ActionListener(){
            String nomeJogador1;
            String nomeJogador2;
            public void actionPerformed(ActionEvent e){
                nomeJogador1= textoNome1.getText();
                nomeJogador2=textoNome2.getText();

                TelaJogo j = new TelaJogo();
                j.construir(nomeJogador1,nomeJogador2);
                janela.dispose();
            }
        });
    }
}
